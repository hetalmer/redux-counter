import * as actionList from "./action-type";
const initialState = {
	count: 0,
	counter: [],
};
export const Reducer = (state = initialState, action) => {
	switch (action.type) {
		case actionList.CREATECHILD:
			return {
				...state,
				count: state.count + 1,
				counter: [...state.counter, { id: state.count, value: 0 }],
			};
		case actionList.RESETCHILD:
			return {
				...state,
				counter: state.counter.map((count) => {
					count.value = 0;
					return count;
				}),
			};
		case actionList.INCREMENTCOUNTER:
			return {
				...state,
				counter: state.counter.map((count) => {
					if (count.id === action.payload) {
						count.value = count.value + 1;
					}
					return count;
				}),
			};
		case actionList.DECREMENTCOUNTER:
			return {
				...state,
				counter: state.counter.map((count) => {
					if (count.id === action.payload) {
						count.value = count.value - 1;
					}
					return count;
				}),
			};
		case actionList.DELETECOUNTER:
			return {
				...state,
				counter: state.counter.filter((count) => count.id !== action.payload),
			};
		default:
			return state;
	}
};
