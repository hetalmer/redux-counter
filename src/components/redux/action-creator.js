import * as action from "./action-type";
export const createChild = () => {
	return {
		type: action.CREATECHILD,
	};
};
export const resetChild = () => {
	return {
		type: action.RESETCHILD,
	};
};
export const incrementCounter = (id) => {
	return {
		type: action.INCREMENTCOUNTER,
		payload: id,
	};
};
export const decrementCounter = (id) => {
	return {
		type: action.DECREMENTCOUNTER,
		payload: id,
	};
};
export const deleteCounter = (id) => {
	return {
		type: action.DELETECOUNTER,
		payload: id,
	};
};
