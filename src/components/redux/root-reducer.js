import { Reducer } from "./reducer";
import { combineReducers } from "redux";

export const rootReducer = combineReducers({
	counterReducer: Reducer,
});
